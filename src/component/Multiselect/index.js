import React from 'react';


class Multiselect extends React.Component {
    constructor(props) {
        
        super(props);
        this.state = {
            noneSelected: '',
            departbox: false,
        };
    }
    componentDidMount() {
        document.addEventListener('click', (e) => {
            if (!this.dropBox.contains(e.target) ) {
                this.setState({
                    departbox: false
                })
            }
        })
    }

    componentWillUnmount(){

    }
    
    showDepart() {
        this.setState({
            departbox: true
        })
    }

    handleChange = (e) => {
       e.stopPropagation()
        debugger
        const {input:{onChange, value}} = this.props;
        let departmentSelected = [...value];
        if (e.target.checked) {
            departmentSelected.push(e.target.value)
        } else{
            var position= departmentSelected.indexOf(e.target.value);
            if(position > -1){               
            departmentSelected.splice(position, 1)
            }
        }
        // onChange(departmentSelected)
        this.props.input.onChange(departmentSelected)
    }

    render() {
        const {input:{onChange, value}, departments,meta: { touched, error, warning }} = this.props;
       
        let departmentSelected = value || []
        return (
            <div>
                <div 
                ref={node => { if(node)( this.dropBox = node) }}
                >
                    <div className={`form-control ${touched && error ? 'alert-danger': ''}`}  onClick={this.showDepart.bind(this)} >
                        {departmentSelected.length > 0 ? departmentSelected.map((v,i) => <span key={i} className='badge-depart'>{v}</span>)  : ' None Selected' }   <span className="caret"></span> 
                       
                    </div>
                    <div className={`depart-box ${this.state.departbox ? 'block' : 'none'}`} >
                        <ul className="m-0 p-0">
                            {
                                departments.map((name, i) => (
                                    <li key={i} > 
                                        <label className="m-0 w-100">
                                            <input type="checkbox" className='inputName' onChange={this.handleChange} value={name}/>{name}
                                        </label> 
                                    </li>
                                ))
                            }
                        </ul>
                    </div>
                    {touched &&
                    ((error && <div className="custom-error">{error}</div>) ||
                        (warning && <div >{warning}</div>))}
                  
                </div>
                </div>
        );
    }
}
export default Multiselect;