import React from 'react';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";

const CustomRoute = ({ component: Component, to, ...rest}) =>(
    <Route
    {...rest}
    render ={props => (
        (<Component {...props} {...rest}/>)
    )}/>
);

export default CustomRoute;