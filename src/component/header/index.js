import React, { Component } from 'react';
import './index.css';
// import logoComp from '../../images/sdg-logo.png';
import youTube from '../../images/yt.PNG';
import Glogo from '../../images/g.PNG';
import fB from '../../images/fb.PNG';
import tweet from '../../images/tweet.PNG';
import linkin from '../../images/lin.PNG';
import gPlus from '../../images/gplus.PNG';

class Header extends Component {   
   
    render() { 
        return (
             <div className="wrapper text-center">
            <div className="back-show"></div>
        <header>
            
            <div className="header d-flex">
                <a className="navbar-brand flex-grow ml-3" href="#"><img src='' alt="-logo"/></a>
                <div className="logo-holds">
                    <ul className="logo-list">
                        <li><a href=""><img src={youTube}   alt="yt-logo"/></a></li>
                        <li><a href=""><img src={Glogo} alt="g-logo"/></a></li>
                        <li><a href=""><img src={fB} alt="fb-logo"/></a></li>
                        <li><a href=""><img src={tweet}alt="t-logo"/></a></li>
                        <li><a href=""><img src={linkin} alt="lin-logo"/></a></li>
                        <li><a href=""><img src={gPlus} alt="gplus-logo"/></a></li>
                        </ul>
                </div>
            </div>
          
        </header>
        </div> );
    }
}
 
export default Header;