import React, { Component } from 'react';
import { Router, Route, Switch } from "react-router";

class ThankScreen extends Component {
  constructor(props) {
    super(props)
    this.state = {
      thankScreen: this.props.thankScreen
    }
  }
  render() {
    return (
      <div className="thanks-screen text-center" id="thanks-screen">
        <div className="alert alert-success">  Thanks for registrating with us, We will get back to you shortly.</div>
        <p>
          <button className="btn btn-primary"
            onClick={() => this.props.revertingThankScreenToFalse()}
          >
            Back</button>

        </p>
      </div>
    );
  }
}

export default ThankScreen;
