import React, { Component } from 'react';
import { Field, reduxForm } from 'redux-form';
import * as FieldValidator from '../../utils/formvalidator';
import { renderField } from '../../utils/renderField';
import { RenderDateField } from '../../utils/renderDateField';
import "react-datepicker/dist/react-datepicker.css";
import Multiselect from '../../component/Multiselect';
import 'react-widgets/dist/css/react-widgets.css';
import calender from '../../images/calendar-icon.png';
import ThankScreen from '../../component/thankScreen';
import { validate } from '../../utils/validate';


class RegForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      firstName: '',
      lastName: '',
      contact: '',
      email: '',
      dateOfRegistration: '',
      address: '',
      departments: [
        'Admin', 'Finance', 'IT', 'Operation'
      ],
      comments: '',
      selectedDepart: [],
      startDate: '',
      thankScreen: false
    }
  }
  componentDidMount() {
    const { initialize } = this.props;
    const values = {
      depart: [],

    };
    initialize(values);
  }

  componentWillUnmount(){
 

    this.setState({
     departments : this.state.departments.length =0
    })
  }

  handleChange = date => {
    this.setState({
      startDate: date
    });
  };

  handleSubmit = event => {
    if (this.props.valid)
      this.setState({
        thankScreen: true
      })
    else 
    this.props.handleSubmit();
    event.preventDefault();
  }

  revertingThankScreenToFalse = () => {
    this.setState({
      thankScreen: false
    })
    this.props.reset()    
  }

  render() {
    const { pristine, reset, submitting } = this.props
    return (
      <div className="form-wrap">
        <div className="container">

          <h2 className="my-3 text-center">Registration Form</h2>
          {
              this.state.thankScreen ? <ThankScreen revertingThankScreenToFalse={this.revertingThankScreenToFalse} /> :

              <form onSubmit={e => this.handleSubmit(e)}>

              <div className="row">
                <div className="col-xl-6 col-md-6 col-sm-6">
                  <div className="my-2">
                    <Field name="firstname" type="text"
                      component={renderField} label="First Name"
                      validate={[FieldValidator.required, FieldValidator.maxLength15]}
                    />
                  </div>
                </div>

                <div className="col-xl-6 col-md-6 col-sm-6">
                  <div className="my-2">
                    <Field name="lastname" type="text"
                      component={renderField} label='Last Name'
                      validate={[FieldValidator.required, FieldValidator.maxLength15]}
                    />
  
                  </div>
                </div>
              </div>
  
              <div className="row">
                <div className="col-xl-6 col-md-6 col-sm-6">
                  <div className="my-2">
                    <Field name="contact" type="number"
                      component={renderField} label="Contact"
                      validate={[FieldValidator.required, FieldValidator.number, FieldValidator.maxLength15, FieldValidator.contact]}
  
                    />
                  </div>
                </div>
  
                <div className="col-xl-6 col-md-6 col-sm-6">
                  <div className="my-2">
                    <Field name="email" type="text"
                      component={renderField} label="Email"
                      validate={FieldValidator.email}
                      warn={FieldValidator.aol}
                    />
                  </div>
                </div>
  
              </div>
  
              <div className="row">
                <div className="col-xl-6 col-md-6 col-sm-6">
                  <div className="my-2">
                    <label>Date of Registration</label>
                    <Field name="email" type="text" label="Date of Registration"
                      component={props =>
                        <RenderDateField
                          label={props.label}
                          meta={props.meta}
                          selected={this.state.startDate}
                          onChange={this.handleChange}
                          className='form-control w-100 custom-calender'
                        />}
                      validate={FieldValidator.required}
                    />
                    <span className='calender'><img src={calender} /></span>  
                  </div>
                </div>
  
                <div className="col-xl-6 col-md-6 col-sm-6">
                  <div className="my-2">
                    <Field name="address" type="text"
                      component={renderField} label="Address"
                      className='form-control'
                    />
                  </div>
                </div>
              </div>
  
              <div className="row">
                <div className="col-xl-6 col-md-6 col-sm-6">
                  <div className="my-2">
                    <label>Select Department</label>  
                    <Field name="depart" type="text" label=""
                      component={props => <Multiselect {...props} departments={this.state.departments} />} label="depart"
                    />  
                  </div>
                </div>
  
  
                
                  <div className="col-xl-6 col-md-6 col-sm-6">
                    <div className="my-2">
                      <Field name="Comment" type="text"
                        component={renderField} label="Comment"
                        validate={[FieldValidator.required, FieldValidator.maxLength100]} className='form-control'
                      />
                      <small><i>(Max 100 Character)</i></small>
                      
                    </div>
                  </div>  
                  </div>

                  <div className="row float-right">
                  <div className="text-right my-4">
  
                  <button type="button" disabled={pristine || submitting} onClick={reset} className='btn btn-info'>Reset</button>  
  
                  <button type="submit" disabled={submitting} className='btn btn-primary ml-4'>  Submit</button>
                 
                </div>
                </div>
             
  
            </form>
            }
        </div>

      </div>
    )
  }
}

export default reduxForm({
  form: 'departmentForm', // a unique identifier for this form
  validate
})(RegForm);


