import React from 'react';
import { Provider } from 'react-redux';
import store from './store';
import { BrowserRouter as Router, Route, Link, Switch } from "react-router-dom";
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import Footer from './component/footer';
import Header from './component/header';
import RegForm from './container/regform/index';
import registerDept from './utils/registerDept';
import CustomRoute from './component/common/route';




function App() {

  return (
    <Router >
      {/* <Route exact path="/" component={DepartmentForm} /> */}
      <Provider store={store}>
        <div className="App">
          <Header />
          <Switch>
            <CustomRoute
              path='/'
              component={RegForm}
              onSubmit={registerDept}
            />
          </Switch>
          {/* <DepartmentForm onSubmit={handlesubmit}/> */}
          <Footer />
        </div>
      </Provider>
    </Router>
  );
}

export default App;
