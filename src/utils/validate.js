export const validate = (values,department,comment) => {
    const errors = {}
    if (!values.firstName) {
        errors.firstName = 'First Name Required'
    }
    if (!values.lastName) {
        errors.lastName = 'Last Name Required'
    }
    if (!values.email) {
        errors.email = 'Invalid email'
    }
    if (!values.contact) {
        errors.contact = 'Max-15'
    }
    if(!values.address ||  values.address.length < 1 ){
       errors.address = 'Please enter  valid adddress'
    }
    if (!values.depart || values.depart.length < 1) {
        errors.depart = 'please select depart'
    }
     return errors
}