export const required = value => (value || typeof value === 'number' ? undefined : 'Required')
export const maxLength = max => value =>
    value && value.length > max ? `Must be ${max} characters or less` : undefined
export const maxLength100 = maxLength(100)
export const maxLength15 = maxLength(15)

export const minLength = min => value =>
    value && value.length < min ? `Must be ${min} characters or more and Max 100 characters` : undefined

    export const contact = value => value && value.length < 10 || value.length > 15 ? 'Invalid number ' :undefined
export const minLength2 = minLength(2)
export const minLength10 = minLength(10)
export const number = value =>
    value && isNaN(Number(value)) ? 'Must be a number' : undefined
export const minValue = min => value =>
    value && value < min ? `Must be at least ${min}` : undefined
export const minValue10 = minValue(10)
export const email = value =>
   value || (value && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value))
        ? null
        : 'Required'

// export const tooYoung = value =>
//     value && value < 13
//         ? 'You do not meet the minimum age requirement!'
//         : undefined
export const aol = value =>
    value && /.+@aol\.com/.test(value)
        ? 'Really? You still use AOL for your email?'
        : undefined

// export const alphaNumeric = value =>
//     value && /[^a-zA-Z0-9 ]/i.test(value)
//         ? 'Only alphanumeric characters'
//         : undefined

export const phoneNumber = value =>
    value && !/^(0|[1-9][0-9]{9})$/i.test(value)
        ? ''
        : undefined
export const tooOld = value =>
    value && value > 65 ? 'You might be too old for this' : undefined

export const address = value =>
value && value.length < 1 ? 'Please enter  valid adddress' : undefined

export const comment = value =>
value && value.length < 2 ? 'You might be too old for this' : undefined

export const depart = value =>
value && value.length > 0 ? undefined :'please select depart'

export const date = value => value && value.length < 2 ? 'select date' : undefined
    

 export const department = value => value && value.length < 0 ? 'Please Select Department' :undefined


  
