import React from 'react'

export const renderField = ({
    input,
    label,
    type,
    meta: { touched, error, warning }
}) => (
        <div>
            <label>{label}</label>
            <div>
                <input className={touched && error ? 'alert-danger': ''} {...input} placeholder={label} type={type} />
                {touched &&
                    ((error && <div className="custom-error">{error}</div>) ||
                        (warning && <div >{warning}</div>))}
            </div>
        </div>
    )