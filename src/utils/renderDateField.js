import React from 'react';
import DatePicker from "react-datepicker";

export const RenderDateField = ({
    selected,
    onChange,
    label,
    meta: { touched, error, warning }
}) => (
        <div>
            <div>
            <DatePicker 
                className={touched && error ? 'alert-danger': ''} 
                selected={selected}
                onChange={onChange}
              
                dateFormat="MM/dd/yyyy"
                placeholderText="MM/dd/yyyy"
                />
                {touched &&
                    ((error && <div className="custom-error">{error}</div>) ||
                        (warning && <div >{warning}</div>))}
            </div>
        </div>
    )